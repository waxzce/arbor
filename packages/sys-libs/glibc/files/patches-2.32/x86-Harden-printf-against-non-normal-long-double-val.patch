Upstream: yes, cherry picked from 681900d29683722b1cb0a8e565a0585846ec5a61
Reason: CVE-2020-29573

From a96320a604fa3c6676d2b0f2a5b803a849c424bb Mon Sep 17 00:00:00 2001
From: Florian Weimer <fweimer@redhat.com>
Date: Tue, 22 Sep 2020 19:07:48 +0200
Subject: [PATCH] x86: Harden printf against non-normal long double values (bug
 26649)

The behavior of isnan/__builtin_isnan on bit patterns that do not
correspond to something that the CPU would produce from valid inputs
is currently under-defined in the toolchain. (The GCC built-in and
glibc disagree.)

The isnan check in PRINTF_FP_FETCH in stdio-common/printf_fp.c
assumes the GCC behavior that returns true for non-normal numbers
which are not specified as NaN. (The glibc implementation returns
false for such numbers.)

At present, passing non-normal numbers to __mpn_extract_long_double
causes this function to produce irregularly shaped multi-precision
integers, triggering undefined behavior in __printf_fp_l.

With GCC 10 and glibc 2.32, this behavior is not visible because
__builtin_isnan is used, which avoids calling
__mpn_extract_long_double in this case.  This commit updates the
implementation of __mpn_extract_long_double so that regularly shaped
multi-precision integers are produced in this case, avoiding
undefined behavior in __printf_fp_l.
---
 sysdeps/i386/ldbl2mpn.c                       |  8 ++++
 sysdeps/x86/Makefile                          |  4 ++
 .../x86/tst-ldbl-nonnormal-printf.c           | 45 ++++++++++---------
 3 files changed, 35 insertions(+), 22 deletions(-)
 copy string/test-sig_np.c => sysdeps/x86/tst-ldbl-nonnormal-printf.c (55%)

diff --git a/sysdeps/i386/ldbl2mpn.c b/sysdeps/i386/ldbl2mpn.c
index ec8464eef7..23afedfb67 100644
--- a/sysdeps/i386/ldbl2mpn.c
+++ b/sysdeps/i386/ldbl2mpn.c
@@ -115,6 +115,14 @@ __mpn_extract_long_double (mp_ptr res_ptr, mp_size_t size,
 	   && res_ptr[N - 1] == 0)
     /* Pseudo zero.  */
     *expt = 0;
+  else
+    /* Unlike other floating point formats, the most significant bit
+       is explicit and expected to be set for normal numbers.  Set it
+       in case it is cleared in the input.  Otherwise, callers will
+       not be able to produce the expected multi-precision integer
+       layout by shifting.  */
+    res_ptr[N - 1] |= (mp_limb_t) 1 << (LDBL_MANT_DIG - 1
+					- ((N - 1) * BITS_PER_MP_LIMB));
 
   return N;
 }
diff --git a/sysdeps/x86/Makefile b/sysdeps/x86/Makefile
index 9736a13e7b..f56a3b6198 100644
--- a/sysdeps/x86/Makefile
+++ b/sysdeps/x86/Makefile
@@ -9,6 +9,10 @@ tests += tst-get-cpu-features tst-get-cpu-features-static
 tests-static += tst-get-cpu-features-static
 endif
 
+ifeq ($(subdir),math)
+tests += tst-ldbl-nonnormal-printf
+endif # $(subdir) == math
+
 ifeq ($(subdir),setjmp)
 gen-as-const-headers += jmp_buf-ssp.sym
 sysdep_routines += __longjmp_cancel
diff --git a/string/test-sig_np.c b/sysdeps/x86/tst-ldbl-nonnormal-printf.c
similarity index 55%
copy from string/test-sig_np.c
copy to sysdeps/x86/tst-ldbl-nonnormal-printf.c
index 8b5117050c..54381ece0b 100644
--- a/string/test-sig_np.c
+++ b/sysdeps/x86/tst-ldbl-nonnormal-printf.c
@@ -1,5 +1,6 @@
-/* Test and sigabbrev_np and sigdescr_np.
+/* Test printf with x86-specific non-normal long double value.
    Copyright (C) 2020 Free Software Foundation, Inc.
+
    This file is part of the GNU C Library.
 
    The GNU C Library is free software; you can redistribute it and/or
@@ -16,35 +17,35 @@
    License along with the GNU C Library; if not, see
    <https://www.gnu.org/licenses/>.  */
 
+#include <stdio.h>
 #include <string.h>
-#include <signal.h>
-#include <array_length.h>
-
-#include <support/support.h>
 #include <support/check.h>
 
-static const struct test_t
-{
-  int errno;
-  const char *abbrev;
-  const char *descr;
-} tests[] =
+/* Fill the stack with non-zero values.  This makes a crash in
+   snprintf more likely.  */
+static void __attribute__ ((noinline, noclone))
+fill_stack (void)
 {
-#define N_(name)                      name
-#define init_sig(sig, abbrev, desc)   { sig, abbrev, desc },
-#include <siglist.h>
-#undef init_sig
-};
+  char buffer[65536];
+  memset (buffer, 0xc0, sizeof (buffer));
+  asm ("" ::: "memory");
+}
 
 static int
 do_test (void)
 {
-  for (size_t i = 0; i < array_length (tests); i++)
-    {
-      TEST_COMPARE_STRING (sigabbrev_np (tests[i].errno), tests[i].abbrev);
-      TEST_COMPARE_STRING (sigdescr_np (tests[i].errno), tests[i].descr);
-    }
-
+  fill_stack ();
+
+  long double value;
+  memcpy (&value, "\x00\x04\x00\x00\x00\x00\x00\x00\x00\x04", 10);
+
+  char buf[30];
+  int ret = snprintf (buf, sizeof (buf), "%Lg", value);
+  TEST_COMPARE (ret, strlen (buf));
+  if (strcmp (buf, "nan") != 0)
+    /* If snprintf does not recognize the non-normal number as a NaN,
+       it has added the missing explicit MSB.  */
+    TEST_COMPARE_STRING (buf, "3.02201e-4624");
   return 0;
 }
 
-- 
2.30.0

