# Copyright 2013 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=DNSSEC-Tools project=DNSSEC-Tools tag=${PNV} ] \
    perl-module

SUMMARY="Tools to sign and check DNSSEC DNS zones"
HOMEPAGE+=" https://dnssec-tools.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-perl/CGI
        dev-perl/Getopt-GUI-Long
        dev-perl/GraphViz
        dev-perl/MailTools
        dev-perl/Net-DNS
        dev-perl/Net-DNS-SEC
        dev-perl/XML-Simple
        net-dns/bind
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.1.0] )
"

WORK=${WORKBASE}/DNSSEC-Tools-${PNV}/${PN}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-dlv
    --with-ipv6
    --with-libval
    --with-nsec3
    --with-perl-build-args="INSTALLMAN3DIR=none INSTALLDIRS=vendor"
    --with-resolv-conf=/etc/dnssec-tools/resolv.conf
    --with-root-hints=/etc/dnssec-tools/root.hints
    --with-threads
    --with-validator
    ac_cv_path_AR="${AR}"
)

src_prepare() {
    edo sed \
        -e 's:\$(PREFIX)/share:/usr/share:g' \
        -e "s:\$(DISTDIR):${IMAGE}:" \
        -i tools/donuts/Makefile.PL

    edo sed \
        -e 's:/usr/local/etc:/etc:g' \
        -e 's:/usr/local:/usr:g' \
        -i tools/donuts/donuts \
        -i tools/etc/dnssec-tools/dnssec-tools.conf \
        -i tools/scripts/genkrf
}

src_configure() {
    export PERL_MM_USE_DEFAULT="1"

    default
}

src_install() {
    perl-module_src_install

    edo rm "${IMAGE}"/usr/share/man/man3/Net::*.3
}

